const argv = require('yargs')
                .command('create', 'Crear un elemento por hacer', {
                    description: {
                        demand: true,
                        alias: 'd',
                        desc: 'Description the task will make'
                    }
                })
                .command('update', 'update for task', {
                    description: {
                        demand: true,
                        alias: 'd',
                        desc: 'Description for task will make'
                    },
                    complete: {
                        default: true,
                        alias: 'c',
                        desc: 'Mark with complete or pending the task'
                    }
                })
                .command('delete', 'delete for task', {
                    description: {
                        demand: true,
                        alias: 'd',
                        desc: 'Description for task that will delete'
                    }
                })
                .help()
                .argv;

module.exports = {
    argv
}