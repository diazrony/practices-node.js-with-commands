const fs = require('fs')

let listForMake = []

const saveDataBase = () => {
    let data = JSON.stringify(listForMake)

    fs.writeFile('db/data.json',data, (err) => {

        if (err) throw new Error(err)

        console.log('file saved')
    })
}

const loadDB = () => {
    try {
        listForMake = require('../db/data.json');
    } catch (error) {
        listForMake = []
    }
   
}

const create = (description) => {

    loadDB()

    let forMake = {
        description,
        complete: false
    }

    listForMake.push(forMake)
    saveDataBase()
    return forMake
}
const getList = () => {
   loadDB()
   return listForMake
}
const update = (description, complete = true) => {
    loadDB()
    let index = listForMake.findIndex( task => task.description === description)
    if (index >= 0 ){
        listForMake[index].complete = complete
        saveDataBase()
        return true
    }else {
        return false
    }
}
const deleted = (description) => {
    loadDB()
    let index = listForMake.findIndex( task => task.description === description)
     if (index >= 0 ){
        listForMake.splice(index,1)
        saveDataBase()
        return true
    }else {
        return false
    }
}
module.exports = {
    create,
    loadDB,
    getList,
    update,
    deleted
}