//const argv = require('yargs').argv;
const argv = require('./config/yargs').argv;
const forMake = require('./for-make/for-maker');
const colors = require('colors');
let command = argv._[0]

switch (command) {
    case 'create':
        let taskForMake = forMake.create(argv.description)
        console.log(taskForMake)
        break;

    case 'list':
        let list = forMake.getList()
        for (const tarea of list) {
            console.log('**************'.green)
            console.log(`Name of Task is ${tarea.description}`)
            console.log(`State the task ${tarea.complete}`)
            console.log('**************'.green)
        }
        break;
    case 'update':
        let update = forMake.update(argv.description,argv.complete)
        console.log(update)
        break;
    case 'delete':
        let deleted = forMake.deleted(argv.description)
        console.log(deleted)
    break;
    default:
        console.log('Command is undefined')
        break;
}